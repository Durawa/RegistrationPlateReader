from PyQt5.QtWidgets import QGroupBox, QVBoxLayout, QTextEdit


class ResultBox(QGroupBox):
    def __init__(self):
        super(ResultBox, self).__init__("Wynik")

        group_box_layout = QVBoxLayout()
        text_edit = QTextEdit()
        text_edit.setPlaceholderText("Tu pojawi się twój wynik")
        group_box_layout.addWidget(text_edit)
        self.setLayout(group_box_layout)