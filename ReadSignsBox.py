from PyQt5.QtWidgets import QGroupBox, QVBoxLayout, QHBoxLayout, QLabel, QPushButton


class ReadSignsBox(QGroupBox):
    def __init__(self):
        super(ReadSignsBox, self).__init__("Odczytane znaki")

        group_box_layout = QVBoxLayout()
        button_layout = QHBoxLayout()
        group_box_label = QLabel()
        prev_button = QPushButton("Poprzedni")
        next_button = QPushButton("Następny")

        group_box_layout.addWidget(group_box_label)
        button_layout.addWidget(prev_button)
        button_layout.addWidget(next_button)
        group_box_layout.addLayout(button_layout)
        self.setLayout(group_box_layout)