from PyQt5.QtWidgets import QMainWindow, QWidget, QMenuBar, QMenu, QAction, \
    QVBoxLayout, QHBoxLayout, QGroupBox, QLabel, QPushButton, QTextEdit
from ImageInputBox import ImageInputBox
from ReadSignsBox import ReadSignsBox
from ResultBox import ResultBox


class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()

        menuBar = self.menuBar()
        fileBar = menuBar.addMenu("Plik")
        editBar = menuBar.addMenu("Edycja")
        setBar = menuBar.addMenu("Opcje")
        helpBar = menuBar.addMenu("Pomoc")

        central = QWidget()
        primeLayout = QVBoxLayout()
        inputLayout = QHBoxLayout()
        input_box = ImageInputBox()
        signs_box = ReadSignsBox()
        result_box = ResultBox()

        inputLayout.addWidget(input_box)
        inputLayout.addWidget(signs_box)
        primeLayout.addLayout(inputLayout)
        primeLayout.addWidget(result_box)

        central.setLayout(primeLayout)
        self.setCentralWidget(central)






