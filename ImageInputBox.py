from PyQt5.QtWidgets import QGroupBox, QVBoxLayout, QHBoxLayout, QLabel, QPushButton


class ImageInputBox(QGroupBox):
    def __init__(self):
        super(ImageInputBox, self).__init__("Wczytywanie rejestracji")

        group_box_layout = QVBoxLayout()
        button_layout = QHBoxLayout()
        group_box_label = QLabel()
        load_button = QPushButton("Załaduj")
        process_button = QPushButton("Przetwórz")

        group_box_layout.addWidget(group_box_label)
        button_layout.addWidget(load_button)
        button_layout.addWidget(process_button)
        group_box_layout.addLayout(button_layout)
        self.setLayout(group_box_layout)